$(document).ready(function() {
    var acc = $(".accordion");
	var i;

	for (i = 0; i < acc.length; i++) {
	    acc[i].addEventListener("click", function() {
	        /* Toggle between adding and removing the "active" class,
	        to highlight the button that controls the panel */
	        this.classList.toggle("active");

	        /* Toggle between hiding and showing the active panel */
	        var panel = this.nextElementSibling;
	        if (panel.style.display === "block") {
	            panel.style.display = "none";
	        } else {
	            panel.style.display = "block";
	        }
	    });
	}
});

$(document).ready(function(){
    var color = true;
    $("#theme").click(function(){
        if(color) {
            $(".bg-dark").css("background-color", "#343a40");
            $("body").css("background-color", "#00000");
            color = false;
        }
        else {
            $(".bg-dark").css("background-color", "#00000");
            $("body").css("background-color", "#ffffff");
            color = true;
        }
    });
});
