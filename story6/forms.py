from django import forms

class Form(forms.Form):
	error_messages = {
		'required': 'This field is required',
	}
	attrs = {
		'class': 'status-form-textarea',
		'type': 'text',
	}
    
	status = forms.CharField(label='Status', required=True, max_length=300, widget=forms.TextInput(attrs=attrs))

class subs(forms.Form):
	email_attrs = {
		'type' : 'email',
		'placeholder' : 'kosong',
		'class' : 'form-control'
	}
	name_attrs = {
		'type' : 'text',
		'placeholder' : 'kosong',
		'class' : 'form-control'
	}
	pass_attrs = {
		'type' : 'password',
		'placeholder' : 'kosong',
		'class' : 'form-control'
	}
	email = forms.EmailField(label='Email', required=True, widget=forms.TextInput(attrs=email_attrs))
	name = forms.CharField(label='Name', required=True, max_length=30, widget=forms.TextInput(attrs=name_attrs))
	password = forms.CharField(label='Password', required=True, max_length=20, widget=forms.TextInput(attrs=pass_attrs))		