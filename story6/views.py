from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from .forms import Form, subs
from .models import Status_Form, subscriberModel
import requests
from django.db import IntegrityError
# Create your views here.

response = {}

def index(request):
	result = Status_Form.objects.all()
	if request.method == 'POST':
		form = Form(request.POST)
		if form.is_valid():
			status = request.POST['status']
			create_stat = Status_Form.objects.create(status=status) 
			return render(request, 'hello.html', {'form' : form, 'status':result})
	else:
		form = Form()
	return render(request, 'hello.html', {'form' : form, 'status':result})

def profile(request):
	return render(request,'profile.html')

def books(request):
	return render(request, 'books.html', response)

def read(request):
	link = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting').json()
	return JsonResponse(link)

def subscribe(request):
	form = subs(request.POST)
	return render(request, 'subscribe.html', {'form':form})

def aaa(request):
	if request.method == 'POST':
		email = request.POST['email']
		name = request.POST['name']
		password = request.POST['password']
		try:
			data = subscriberModel.objects.create(email=email, name=name, password=password)
		except IntegrityError as e:
			return JsonResponse({'status' : False}) 
	return JsonResponse({'email': email, 'name':name, 'status':True})

def validate(request):
	if request.method == 'POST':
		email = request.POST['email']
		check = subscriberModel.objects.filter(email=email)
		if check.exist():
			return JsonResponse({'exist':True})
		else:
			return JsonResponse({'exist':False})