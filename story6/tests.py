from django.test import TestCase, Client
from django.http import HttpRequest
from .views import index,profile
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.color import Color

# Create your tests here.
class Lab6UnitTest(TestCase):

	def test_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_contains_helloapakabar(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Hello, apa kabar?', html_response)

	def test_contains_name(self):
		request = HttpRequest()
		response = profile(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Shania Nabilah', html_response)

	def test_contains_npm(self):
		request = HttpRequest()
		response = profile(request)
		html_response = response.content.decode('utf8')
		self.assertIn('1706044175', html_response)


class FunctionalTest(TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(FunctionalTest, self).setUp()

	def tearDown(self):
		self.driver.quit()
		super(FunctionalTest, self).tearDown()

	def test_input_todo(self):
		driver = self.driver
        # Opening the link we want to test
		driver.get('http://localhost:8000/')
        # find the form element
		search_box = driver.find_element_by_name('status')
        # Fill the form with data
		search_box.send_keys('Coba Coba')
        # submitting the form
		search_box.submit()
		self.assertIn('Coba Coba', driver.page_source)

	def test_layout_css(self):
		driver = self.driver
		driver.get('http://helloshan.herokuapp.com')
		text = driver.find_element_by_tag_name('h2').text
		button = driver.find_element_by_tag_name('button').text
		self.assertIn('Hello, apa kabar?', text)
		self.assertIn('Submit', button)
		btn = driver.find_element_by_tag_name("button").value_of_css_property('background-color')
		color = Color.from_string(btn).hex
		self.assertEqual('#007bff', color)
		navbar = driver.find_element_by_tag_name("nav").value_of_css_property('background-color')
		navbar_color = Color.from_string(navbar).hex
		self.assertEqual('#343a40', navbar_color)

        








	