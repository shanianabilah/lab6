$(document).ready(function() {
    var acc = $(".accordion");
	var i;
	for (i = 0; i < acc.length; i++) {
	    acc[i].addEventListener("click", function() {
	        /* Toggle between adding and removing the "active" class,
	        to highlight the button that controls the panel */
	        this.classList.toggle("active");

	        /* Toggle between hiding and showing the active panel */
	        var panel = this.nextElementSibling;
	        if (panel.style.display == "block") {
	            panel.style.display = "none";
	        } else {
	            panel.style.display = "block";
	        }
	    });
	}

	var color = true;
    $("#theme").click(function(){
        if(color) {
            $("body").css("background-color", "#343a40");
            color = false;
        }
        else {
            $("body").css("background-color", "#ffffff");
            color = true;
        }
    });

    $.fn.center = function () {
  		this.css("position","absolute");
  		this.css("top", Math.max(0, (
    		($(window).height() - $(this).outerHeight()) / 2) + 
     		$(window).scrollTop()) + "px"
  		);
  		this.css("left", Math.max(0, (
    		($(window).width() - $(this).outerWidth()) / 2) + 
     		$(window).scrollLeft()) + "px"
  		);
  		return this;
	}

	$("#overlay").show();
	$("#overlay-content").show().center();

	setTimeout(function(){
  		$("#overlay").fadeOut();
	}, 2000);

  $(function(){
    $.ajax({
      type : "GET",
      url: '/data',
      dataType: 'json',
      success: function(data){
        var result = ''
        var temp = data['items']
        for (var i = 0; i<temp.length; i++) {
          item = temp[i]
          var img = item['volumeInfo']['imageLinks']['smallThumbnail'];
          var title = item['volumeInfo']['title']
          var author = item['volumeInfo']['authors']
          var desc = item['volumeInfo']['description']

          result += "<tr><td><img src =" + img + "></td>"
          result += "<td>" + title + "</td>"
          result += "<td>" + author + "</td>"
          result += "<td>" + desc + "</td>"
          result += "<td><button type='button' class='tombol btn btn-dark'>Favorite</button></td></tr>"
        }
        $("#bt").html(result)
      }
    });
  });
  var fav = 0;
  $(document).on('click', '.tombol', function(){
    if ($(this).hasClass('clicked')) {
      fav--;
      $(this).removeClass('clicked');
      $(this).css("background-color","#343a40");
      $(this).css("color", "white");
    }
    else{
      fav++;
      $(this).addClass('clicked');
      $(this).css("background-color", "yellow");
      $(this).css("color", "black");
    }
    $("#jml").html(fav);
  });

  $(document).ready(function () {
      $('#id_email').bind('input', function () {
        //ambil value dari input field
        var val = $(this).val();
        var csrf = $("[name=csrfmiddlewaretoken]").val();
        $.ajax({
          method: 'POST',
          url: 'validate',
          headers: {
            "X-CSRFToken": csrf,
          },
          data: {
            email = $('#id_email').val(),
          },
          success: function (data) {
            if (data.exist === true &&) {
              $("#sub-btn").hide();
            } else {
              $("#sub-btn").show();
            }
          }
        })
      })
  
});