from django.contrib import admin
from django.conf.urls import url
from story6.views import index, profile, books, read, subscribe, aaa, validate

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^status', index, name='index'),
    url(r'^profile', profile, name = 'profile'),
    url(r'^books', books, name = 'books'),
    url(r'^data', read, name = 'read'),
    url(r'^subs', subscribe, name = 'subscribe'),
    url(r'^aaa', aaa, name='aaa'),
    url(r'^validate', validate, name='validate')
]
